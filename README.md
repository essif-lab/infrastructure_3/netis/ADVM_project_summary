# Project Summary Authority Delegation & Verifiable Mandates (AD/VM)

**Are you interested in cooperation? Would you like to participate in the interoperability testing which will soon be active? Please contact sebastjan.pirih@netis.si.**

With this project, we will address the SSI aspect of Authority Delegation (AD) or
Verifiable Mandates (VM) – as referred within EBSI/ESSIF. VM are fundamental for the broad
adoption of SSI by businesses (e.g. mandate employees) & individuals (e.g. parents
representing children). However, there are no detailed governance frameworks and outlined
process flows, which cover the VMs, particularly no solutions aligned with ESSIF as well
as legacy frameworks like eIDAS. Furthermore, there are no viable tech solutions for it,
which would enable simple usage.

The project aims to provide the following deliverables: 

> **(1) research paper on VM; <br>
> (2) tech specs, including (2.1) data models and schemas for VMs, (2.2) Flows for issuance,
> verification and revocation based on eIDAS levels of assurances and leveraging ESSIF
> trusted registries,  <br>
> (3) Implementation of an open-source solution (SDK/lib in Java/Kotlin) incl. a user interface,  <br>
> (4) wallet integrations and  <br>
> (5) demo.**



## Our component

1. Outlined data models, schemas & flows around SSI delegation processes (issuing,
revoking etc.)
2. Library that enables delegation & verifiable mandates (via enterprise & consumer
wallets)
3. A demo user interface (UI)

## Technology description

Our innovation will be multi-fold and will contribute to 1) the governance of authority
delegations (AD), 2) with defined models, schemas, as well as 3) a technical solution. The
latter will be a library, which will enable the core functions for ADs (secure and
user-friendly management of ADs in the form of VCs) and which will be usable in other IT
solutions (e.g. android mobile wallet, java-based enterprise wallets). We will build on
the “LetsTrust.org” (SSI Fabric GmbH) library, which already supports the core SSI
functionalities & is EBSI/ESSIF compliant. The lib provides OSS SDKs & libraries in
Java/Kotlin. Furthermore, we will demonstrate the innovation of SSI cloud-based enterprise
& consumer edge agents. For the former we will build on the Blockchain Lab:UM’s SSI
Enterprise Agent also used within the DE4A (EBSI Early Adopter). It is a deployable
java-based solution incorporating HL Aries (Go). For the consumer edge agent we will build
on Netis’s user wallet.

## High-level architecture recap

![](./high-level-architecture-recap.png) 
